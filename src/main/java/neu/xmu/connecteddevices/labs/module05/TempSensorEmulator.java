package neu.xmu.connecteddevices.labs.module05;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.labbenchstudios.edu.connecteddevices.common.DevicePollingManager;
import neu.xmu.connecteddevices.common.DataUtil;
import neu.xmu.connecteddevices.common.SensorData;

public class TempSensorEmulator implements Runnable{
	
	private static final Logger _Logger =
			Logger.getLogger(TempSimulatorApp.class.getSimpleName());
	private long _pollCycle;
	private long pollCycle;
	private DevicePollingManager _pollManager;
	private static DataUtil dataUtil = new DataUtil();
	public TempSensorEmulator(long pollCycle){
		
		super();
		
		if (pollCycle >= 1L) {
			_pollCycle = pollCycle;
		}
		
		_pollManager = new DevicePollingManager(1);
	}

	public TempSensorEmulator(String string, long _pollCycle) {
		string = string;
		_pollCycle = pollCycle;
	}

	//start thread.
	public void startPolling(){
		_Logger.info("Creating and scheduling SensorData poller...");
		_pollManager.schedulePollingTask(
				new TempSensorEmulator("Read json file", _pollCycle), _pollCycle);
	}
	  
	// A thread to read the JSON data written Python application 
	// and convert the JSON data read from the file into a SensorData instance.
	// Then, convert the SensorData back to JSON and write the output to the console.
	public void run() {
		try {
			SensorData sensordata = dataUtil.toSensorDataFromJsonFile("/Users/Cassie/eclipse-workspace/iot-device/Data/Sensordata.json");
			dataUtil.toJsonFromSensorData(sensordata);
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
