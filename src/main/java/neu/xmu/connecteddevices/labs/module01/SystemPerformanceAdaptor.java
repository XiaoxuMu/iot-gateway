package neu.xmu.connecteddevices.labs.module01;

import java.util.logging.Logger;

import com.labbenchstudios.edu.connecteddevices.common.DevicePollingManager;

public class SystemPerformanceAdaptor {
	
	private long _pollCycle;
	private DevicePollingManager _pollManager;
	private static final Logger _Logger =
			Logger.getLogger(SystemPerformanceApp.class.getSimpleName());
	
	public SystemPerformanceAdaptor(long pollCycle){
		
		super();
		
		if (pollCycle >= 1L) {
			_pollCycle = pollCycle;
		}
		
		_pollManager = new DevicePollingManager(2);
	}

	//start two threads.
	public void startPolling(){
		_Logger.info("Creating and scheduling CPU Utilization poller...");
		_pollManager.schedulePollingTask(
				new SystemCpuUtilTask("CPU Utilization", _pollCycle), _pollCycle);
		_Logger.info("Creating and scheduling Memory Utilization poller...");
		_pollManager.schedulePollingTask(new SystemMemUtilTask("Memory Utilization", _pollCycle), _pollCycle);
	}
}

