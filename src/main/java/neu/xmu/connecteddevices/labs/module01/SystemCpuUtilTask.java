package neu.xmu.connecteddevices.labs.module01;

import java.lang.management.ManagementFactory;
import java.util.Date;
import java.text.SimpleDateFormat;

import com.sun.management.OperatingSystemMXBean;

public class SystemCpuUtilTask implements Runnable {
	
	private long pollCycle;
	
	//Constructor
	public SystemCpuUtilTask(String string, long _pollCycle) {
		string = string;
		_pollCycle = pollCycle;
	}

	//get the CPU usage data
	public static float getDataFromSensor() {
		OperatingSystemMXBean os = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
		double cpupercent = os.getSystemCpuLoad();
		return (float) cpupercent * 100;
	}
	
	//output the CPU data
	public void run() {
		Date date = new Date();    
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		float cpudata = getDataFromSensor();
		System.out.println(format.format(date) + " INFO: " + "CPU Utilization = " + cpudata);	
	}

	
	
}
