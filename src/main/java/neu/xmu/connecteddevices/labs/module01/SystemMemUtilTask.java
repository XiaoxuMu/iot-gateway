package neu.xmu.connecteddevices.labs.module01;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import com.sun.management.OperatingSystemMXBean;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SystemMemUtilTask implements Runnable {

	private long pollCycle;

	//Constructor
	public SystemMemUtilTask(String string, long _pollCycle) {
		string = string;
		_pollCycle = pollCycle;
	}

	//get the memory usage data
	public static float getDataFromSensor() {
		OperatingSystemMXBean os = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
		long physicalMemory = os.getTotalPhysicalMemorySize() - os.getFreePhysicalMemorySize();
		long totalmemory = os.getTotalPhysicalMemorySize();
		float percentage = (float)(physicalMemory * 100.0 / totalmemory + 0.5);
		return percentage;
	}
	
	//output the memory data
	public void run() {
		Date date = new Date();    
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		float Memdata = getDataFromSensor();
		System.out.println(format.format(date) + " INFO: " + "Memory Utilization = " + Memdata);
		
	}

}
