/**
 * 
 */
package neu.xmu.connecteddevices.labs.module01;

import java.util.logging.Logger;

import com.labbenchstudios.edu.connecteddevices.common.BaseDeviceApp;
import com.labbenchstudios.edu.connecteddevices.common.DeviceApplicationException;


/**
 *
 */
public class SystemPerformanceApp extends BaseDeviceApp
{
	// static
	
	private static final Logger _Logger =
		Logger.getLogger(SystemPerformanceApp.class.getSimpleName());
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		SystemPerformanceApp app = new SystemPerformanceApp(SystemPerformanceApp.class.getSimpleName(), args);
		app.startApp();
	}
	
	// private var's
	
	private SystemPerformanceAdaptor _sysPerfAdaptor;
	
	// constructors
	
	/**
	 * Default.
	 * 
	 */
	public SystemPerformanceApp()
	{
		super();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param appName
	 */
	public SystemPerformanceApp(String appName)
	{
		super(appName);
	}
	
	/**
	 * Constructor.
	 * 
	 * @param appName
	 * @param args
	 */
	public SystemPerformanceApp(String appName, String[] args)
	{
		super(appName, args);
	}
	
	// protected methods
	
	/* (non-Javadoc)
	 * @see com.labbenchstudios.edu.connecteddevices.common.BaseDeviceApp#start()
	 */
	@Override
	protected void start() throws DeviceApplicationException
	{
		SystemPerformanceAdaptor adaptor = new SystemPerformanceAdaptor(8L);
		adaptor.startPolling();
		_Logger.info("Hello - module01 here!");
		//Get the performance data every 8 seconds.
		
	}
	
	/* (non-Javadoc)
	 * @see com.labbenchstudios.edu.connecteddevices.common.BaseDeviceApp#stop()
	 */
	@Override
	protected void stop() throws DeviceApplicationException
	{
		_Logger.info("Stopping module01 app...");
	}
	
}
