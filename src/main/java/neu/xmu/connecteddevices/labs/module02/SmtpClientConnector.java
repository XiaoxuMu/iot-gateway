package neu.xmu.connecteddevices.labs.module02;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

import com.labbenchstudios.edu.connecteddevices.common.ServiceResourceInfo;

/**
 * SmtpClientConnector class implements an SMTP send with text data to an e-mail address.
 * It be configured via the data stored in ConfigUtil from the ConnectedDevicesConfig.props. 
 * @author Xiaoxu Mu
 */
public class SmtpClientConnector{
	
	public void publishMessage(ServiceResourceInfo resource, int qosLevel,String text){    
		// Sender's email ID needs to be mentioned
		String from = "xiaoxu0403@gmail.com";
		String pass = "nuigkomascbfbykh";
		
		// Recipient's email ID needs to be mentioned.
		String to = "4deviceclass@gmail.com";
		String host = "smtp.gmail.com";
		String port = "465";

		// Get system properties
		Properties properties = System.getProperties();
		
		// Setup mail server
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.user", from);
		properties.put("mail.smtp.password", pass);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.socketFactory.fallback", "false");

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties);

		try{	
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));

			// Set Subject: header field
			message.setSubject("Temperature");

			// Now set the actual message
			message.setText(text);

			// Send message
			Transport transport = session.getTransport("smtp");
			transport.connect(host, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
			System.out.println(text);
		}catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}
}
