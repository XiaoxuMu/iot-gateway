package neu.xmu.connecteddevices.labs.module02;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.labbenchstudios.edu.connecteddevices.common.DevicePollingManager;
import com.labbenchstudios.edu.connecteddevices.common.ServiceResourceInfo;

import neu.xmu.connecteddevices.common.SensorData;
import neu.xmu.connecteddevices.labs.module01.SystemCpuUtilTask;

public class TempSensorEmulator implements Runnable{
	
	private static final Logger _Logger =
			Logger.getLogger(TempSimulatorApp.class.getSimpleName());
	private  SmtpClientConnector _smtpConnector = new SmtpClientConnector();
	private long _pollCycle;
	private long pollCycle;
	private DevicePollingManager _pollManager;
	private static SensorData sensorData = new SensorData();
	private int threshold = 2;
	
	public TempSensorEmulator(long pollCycle){
		
		super();
		
		if (pollCycle >= 1L) {
			_pollCycle = pollCycle;
		}
		
		_pollManager = new DevicePollingManager(1);
	}

	public TempSensorEmulator(String string, long _pollCycle) {
		string = string;
		_pollCycle = pollCycle;
	}

	//start thread.
	public void startPolling(){
		_Logger.info("Creating and scheduling SensorData poller...");
		_pollManager.schedulePollingTask(
				new TempSensorEmulator("CPU Utilization", _pollCycle), _pollCycle);
	}
	
	//To generate values.
	public static float generator() {
		return (float) (Math.random() * 30);
	}
	
	public static String getSensorData() {
		return sensorData.str();
	}
	
	//While running, it will check if the randomly generated value is different from the average 
    //stored measurement by a configurable threshold (5). If the threshold is reached or surpassed, 
    //the most recent SensorData should be emailed to a remote service using the SmtpClientConnector module.    
	public void sendNotification()
    {
           try {
                 sensorData.getValue();
                 ServiceResourceInfo resourceInfo =
                        new ServiceResourceInfo(
                               sensorData.getName(),
                               "Temperature above threshold",
                               sensorData.getName() + "Threshold Crossing",
                               String.valueOf(sensorData.getErrorCode()));
                 if(sensorData.getErrorCode() > threshold) {
                	 System.out.println("\nCurrent temp exceeds average by > " + threshold + ". Converting data...");
                	 _smtpConnector.publishMessage(resourceInfo,1,sensorData.str() );
                 }
           } catch (Exception e) {
                 _Logger.log(Level.WARNING, "Failed to send SMTP message.", e);
           } 
    }

	// A thread to generate a temperature value between 0 C and 30 C
	public void run() {
		TempSensorEmulator emulator = new TempSensorEmulator(10L);
		float temp = emulator.generator();
		sensorData.addValue(temp);
		System.out.println("\n" + sensorData.str());
		emulator.sendNotification();
	}
}
