package neu.xmu.connecteddevices.labs.module08;

//TempSensorCloudPublisherApp is to publish a temperature value 
//to the “TempSensor” variable every minute using HTTPS.
public class TempSensorCloudPublisherApp implements Runnable{
	//static
	private static TempSensorCloudPublisherApp _App;
	
	public static void main(String[] args){
		Thread t = new Thread(new TempSensorCloudPublisherApp());
		t.start();
	}
		    
	// params
	private UbidotsClientConnector _ubidotsClient;
		    
	// constructors
	public TempSensorCloudPublisherApp()
	{
		super(); 
	}
		    
	// public methods
	public void run() {
		while (true) {
			_ubidotsClient = new UbidotsClientConnector();
			_ubidotsClient.connect();
				
			try {
				Thread.sleep(1000 * 60);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
