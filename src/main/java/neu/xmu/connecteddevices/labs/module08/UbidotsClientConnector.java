package neu.xmu.connecteddevices.labs.module08;

import com.ubidots.ApiClient;
import com.ubidots.DataSource;
import com.ubidots.Variable;

public class UbidotsClientConnector {
	
	private String _apiKey = "BBFF-28b6937370ac436d5b84f29818070eb5397";
	private ApiClient _apiClient;
	private Variable _tempSensorVar;

	//Set up HTTPS connection.
	public void connect(){
        String baseUrl = "https://things.ubidots.com/api/v1.6/";
        // this is a Ubidots class
        // need to call 'fromToken()' on ApiClient before updating base URL,
        // as it will create the underlying ServerBridge if not already created
        _apiClient = new ApiClient(_apiKey); 
        
        // once you have a DataSource ref, you can retrieve / update variable data (your topic)
        DataSource dataSource = _apiClient.getDataSource("5dc044941d84724858ad300c");
        Variable[] variables = dataSource.getVariables();
        
        for(Variable var : variables) {
        	if(var.getId().equals("5dc046f31d84724fed2876f1")) {
        	} else if(var.getId().equals("5dc046ea1d84724fed2876ed")) {
        		//refer to TempSensor variable
        		_tempSensorVar = var;
        		double value = Math.random() * 30;
        		_tempSensorVar.saveValue(value);
        		System.out.println("Successfully publish the TempSensor value : " + value);
        	}
        }
        _apiClient.fromToken(_apiKey);
        if (baseUrl != null && baseUrl.length() > 0) {
            _apiClient.setBaseUrl(baseUrl);
        }
    }
}
