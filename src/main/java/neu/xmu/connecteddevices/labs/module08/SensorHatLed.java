package neu.xmu.connecteddevices.labs.module08;

public class SensorHatLed {

	private String displayMsg;
	
	//
	public SensorHatLed() {
		super();
	}
	
	public String setDisplayMessage(String msg) {
		displayMsg = msg;
		return displayMsg;
	}

    public void show_message() {
		System.out.println("SenseHat LED display:" + displayMsg);
	}
}
