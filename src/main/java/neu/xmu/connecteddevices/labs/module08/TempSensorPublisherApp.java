package neu.xmu.connecteddevices.labs.module08;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.eclipse.paho.client.mqttv3.MqttException;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;
import neu.xmu.connecteddevices.labs.module06.MqttClientConnector;

//TempSensorPublisherApp is to publish a temperature value 
//to the "TempSensor" variable every minute using MQTT (ssl). 
public class TempSensorPublisherApp implements Runnable{
	
	private String host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
	private String pemfilename = "/Users/Cassie/eclipse-workspace/iot-gateway/config/ubidots_cert.pem";
	private String token = "BBFF-8uhL2pnuekP3h0LJwsJYiNiP3cUAXi";

	public static void main(String[] args){
		Thread t = new Thread(new TempSensorPublisherApp());
		t.start();
	}
	    
	// params
	private MqttClientConnector _mqttClient;
	
	// public methods
	public void run(){
		while (true) {
			try {
				_mqttClient = new MqttClientConnector(host, token, pemfilename);
				_mqttClient.connect();
				
				//Set up topic name and payload.
				String topicName = "/v1.6/devices/new-device/temp-sensor";
				double tempvalue = Math.random() * 30;
				String payload = String.valueOf(tempvalue);
				
				//Publish message.
				_mqttClient.publishMessage(topicName, 0, payload.getBytes());
				_mqttClient.disconnect();
				
				Thread.sleep(1000 * 60);
				
//				String deviceLabel = "new-device";
//		        String variableLabel = "temp-sensor";
//				String testValue = "28";
//				String payload = "{\"" + variableLabel + "\": " + testValue + "}";
			} catch (KeyManagementException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (KeyStoreException e) {
				e.printStackTrace();
			} catch (CertificateException e) {
				e.printStackTrace();
			} catch (MqttException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} 
	}
}
