package neu.xmu.connecteddevices.labs.module06;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.UUID;
import java.util.logging.Logger;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.xmu.connecteddevices.labs.module08.SensorHatLed;

public class MqttClientConnector implements MqttCallback{
	
	private String _protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	private String _host = ConfigConst.DEFAULT_MQTT_SERVER;
	private int _port = ConfigConst.DEFAULT_MQTT_PORT;
	private String _clientID = UUID.randomUUID().toString();
	public String _brokerAddr;
	private String _pemfilepath;
	private Boolean _isSecure = false;
	private String _userName = "BBFF-8uhL2pnuekP3h0LJwsJYiNiP3cUAXi";
	private String _password = "BBFF-8uhL2pnuekP3h0LJwsJYiNiP3cUAXi";
	private MqttClient _mqttClient;
	
	private static final Logger _Logger =
            Logger.getLogger(MqttClientConnector.class.getName());
	
	//Constructor : default
	public MqttClientConnector() {
		super();
		System.out.println("Using client ID for broker connection: " + _clientID);
		_brokerAddr = _protocol + "://" + _host + ":" + _port;
		System.out.println("Using URL for broker connection: " + _brokerAddr);
	}
	
	//Constructor: SSL
	public MqttClientConnector(String hostname, String token, String pemfilepath){
		super();
		_host = hostname;
		if(pemfilepath != null) {
			File file = new File(pemfilepath);
			if(file.exists()) {
				_protocol = ConfigConst.SECURE_MQTT_PROTOCOL;
				_host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
				_port = ConfigConst.SECURE_MQTT_PORT;
				_pemfilepath = pemfilepath;
				//Set a boolean flag within the connector class to 
				//indicate whether or not TLS is enabled
				_isSecure = true;
				System.out.println("Using secure connention.");
			} else {
				System.out.println("Using default connention.");
			}
		}
		System.out.println("Using client ID for broker connection: " + _clientID);
		_brokerAddr = _protocol + "://" + _host + ":" + _port;
		System.out.println("Using URL for broker connection: " + _brokerAddr);
	}
	
	//Build a connection with server
	public void connect() throws MqttException, KeyManagementException, NoSuchAlgorithmException, 
						  		 KeyStoreException, CertificateException, IOException {
		if(_mqttClient == null) {
			MemoryPersistence persistence = new MemoryPersistence();
			_mqttClient = new MqttClient(_brokerAddr, _clientID, persistence);
			
			MqttConnectOptions options = new MqttConnectOptions();
			options.setCleanSession(true);
			options.setKeepAliveInterval(120);
	
			if(_isSecure == true) {
				options.setUserName(_userName);
				options.setPassword(_password.toCharArray());
				secureConnect(options);
			}
	
			System.out.println("Connecting to broker: " + _brokerAddr);
			_mqttClient.setCallback(this);
			_mqttClient.connect(options);
			System.out.println("Connected to broker: " + _brokerAddr);
		}	
	}
	
	//Set up TLS/SSL support
	private void secureConnect(MqttConnectOptions option) 
			throws NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException, KeyManagementException {
		System.out.println("Configuring TLS...");
		
		_protocol = ConfigConst.SECURE_MQTT_PROTOCOL;
		
		SSLContext sslcontext = SSLContext.getInstance(_protocol);
		KeyStore keystore = readKeyStore(_pemfilepath);
		TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init(keystore);
		sslcontext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());

		MqttConnectOptions options = new MqttConnectOptions();
		options.setSocketFactory(sslcontext.getSocketFactory());
	}
	
	//Read certificate from Keystore.
	private KeyStore readKeyStore(String pemfilepath) 
			throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
		KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
		FileInputStream fileinput = new FileInputStream(pemfilepath);
		BufferedInputStream bufferedinput =  new BufferedInputStream(fileinput);
		CertificateFactory certfac = CertificateFactory.getInstance("X.509");
		keystore.load(null);
		while(bufferedinput.available() > 0) {
			Certificate cert = certfac.generateCertificate(bufferedinput);
			keystore.setCertificateEntry("adk_store" + bufferedinput.available(), cert);
		}
		return keystore;
	}
	
	//Subscribe to a specific topic
	public void subscribeToTopic(String topicName, int qos) throws MqttException {
		_mqttClient.subscribe(topicName, qos);
		System.out.println("Subscribe to topic: " + topicName);
	}

	//Publish the topic and payload to broker
	public void publishMessage(String topicName, int qos, byte[] bytes) 
			throws MqttPersistenceException, MqttException {
		System.out.println("Publishing message to topic: " + topicName);
		//create a new MqttMessage and pass payload to the constructor.
		MqttMessage msg = new MqttMessage(bytes);
		//set qos level
		msg.setQos(qos);
		msg.setRetained(true);
		_mqttClient.publish(topicName, msg);
	}
	
	//Disconnect the connection
	public void disconnect() throws MqttException {
		_mqttClient.disconnect();
		System.out.println("Disconnected with broker: " + _brokerAddr + "\n");
	}

	public void connectionLost(Throwable cause) {
		System.out.println("Connection to MQTT broker lost!");
	}
	
	//If message arrived from the broker,output the topic name and payload.
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		String payload = new String(message.getPayload());
		System.out.println("Message received:\n\ttopic: "+ topic + "\n\tpayload: " + payload);
		//Display the value from the message payload to the SenseHat LED display
		SensorHatLed sensorhat = new SensorHatLed();
		String msg = sensorhat.setDisplayMessage(payload);
		sensorhat.show_message();
	}

	//If the message is delivered, output a token.
	public void deliveryComplete(IMqttDeliveryToken token) {
		try {
			System.out.println("Delivery completed: "+ token.getMessageId() +
								" - " + token.getResponse() + " - " + token.getMessage());
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}
}
