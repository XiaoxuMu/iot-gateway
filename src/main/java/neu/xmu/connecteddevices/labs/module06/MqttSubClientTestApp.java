package neu.xmu.connecteddevices.labs.module06;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.eclipse.paho.client.mqttv3.MqttException;

import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.xmu.connecteddevices.labs.module06.MqttClientConnector;

public class MqttSubClientTestApp {

	//static
	private static MqttSubClientTestApp _App;
	
    public static void main(String[] args)
    {
       _App = new MqttSubClientTestApp();
       try {
              _App.start();
       } catch (Exception e) {
              e.printStackTrace();
       }
    }
    
    // params
    private MqttClientConnector _mqttClient;
    
    // constructors
    public MqttSubClientTestApp()
    {
    	super(); 
    }
    
    // public methods
    public void start() throws MqttException, KeyManagementException, 
    						   NoSuchAlgorithmException, KeyStoreException,
    						   CertificateException, IOException{
       _mqttClient = new MqttClientConnector();
       _mqttClient.connect();
       
       //Set up topic name.
       String topicName = "Temperature";
       // Subscribe to topic.
       _mqttClient.subscribeToTopic(topicName,2); 
   }
}
