package neu.xmu.connecteddevices.labs.module06;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.eclipse.paho.client.mqttv3.MqttException;

import com.google.gson.JsonIOException;

import neu.xmu.connecteddevices.common.DataUtil;
import neu.xmu.connecteddevices.common.SensorData;
import neu.xmu.connecteddevices.labs.module02.TempSensorEmulator;

//This is the Publisher application.
public class MqttPubClientTestApp {
		private static MqttPubClientTestApp _App;
        
        public static void main(String[] args)
        {
           _App = new MqttPubClientTestApp();
           try {
                  _App.start();
           } catch (Exception e) {
                  e.printStackTrace();
           } 
        }
        
        // params
        private MqttClientConnector _mqttClient;
        
        // constructors
        public MqttPubClientTestApp()
        {
        	super(); 
        }
        
        // public methods
        // Connect to the MQTT client and publish a test message for a topic
        public void start() throws MqttException, JsonIOException, IOException, 
        						   KeyManagementException, NoSuchAlgorithmException, 
        						   KeyStoreException, CertificateException{
           _mqttClient = new MqttClientConnector();
           _mqttClient.connect();
           // set up a topic name 
           String topicName = "Temperature";
           // set up payload.
           DataUtil dataUtil = new DataUtil();
           SensorData sensordata = new SensorData();
           TempSensorEmulator emulator = new TempSensorEmulator(10L);
           float temp = emulator.generator();
           sensordata.addValue(temp);
           String payload = dataUtil.toJsonFromSensorData(sensordata);
           System.out.println("Payload: " + payload);
           // publish message
           _mqttClient.publishMessage(topicName, 2, payload.getBytes());
           _mqttClient.disconnect();
       }
 }

