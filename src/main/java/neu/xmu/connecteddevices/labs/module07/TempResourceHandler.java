package neu.xmu.connecteddevices.labs.module07;

import java.io.IOException;
import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.CoAP.Type;
import org.eclipse.californium.core.coap.EmptyMessage;
import org.eclipse.californium.core.server.resources.CoapExchange;

import com.google.gson.JsonIOException;

import neu.xmu.connecteddevices.common.DataUtil;
import neu.xmu.connecteddevices.common.SensorData;
import neu.xmu.connecteddevices.labs.module02.TempSensorEmulator;

//TempResourceHandler class is to handle the following requests from the client:
//1. Ping
//2. GET
//3. POST 
//4. PUT
//5. DELETE
public class TempResourceHandler extends CoapResource{
	
	private static final Logger _Logger =
            Logger.getLogger(TempResourceHandler.class.getName());
	
//	private String content;
	
	//Constructors
	public TempResourceHandler() throws JsonIOException, IOException {
		super("temp", true);
//		content = setcontent();
	}
	
	//Set up resource name and payload.
	public TempResourceHandler(String name) throws JsonIOException, IOException {
		super(name);
//		content = setcontent();
		getAttributes().setTitle("temp");
		getAttributes().setObservable();
		setObservable(true);
		setObserveType(CoAP.Type.CON);
//		_Logger.info(content);
	}
	
	//Using SensorData class and DataUtil class to create a simple SensorData instance
	public String setcontent() throws JsonIOException, IOException {
		DataUtil dataUtil = new DataUtil();
        SensorData sensordata = new SensorData();
        TempSensorEmulator emulator = new TempSensorEmulator(10L);
        float temp = emulator.generator();
        sensordata.addValue(temp);
        String payload = dataUtil.toJsonFromSensorData(sensordata);
        return payload;
	}
	
	//Convert JSON to SensorData instance.
	public SensorData converJsontoSensorData(String data) {
		DataUtil datautil = new DataUtil();
		SensorData sensordata = datautil.toSensorDataFromJson(data);
		_Logger.info("Convert Json to SensorData Instance:\n\t" + sensordata.str());
		return sensordata;
	}
	
	//Convert the SensorData instance to JSON
	public String converSensorDatatoJson(SensorData convert) throws JsonIOException, IOException {
		DataUtil datautil = new DataUtil();
		String jsondata = datautil.toJsonFromSensorData(convert);
		_Logger.info("Convert SensorData Instance to Json:\n\t" + jsondata);
		return jsondata;
	}
	
	// Handle GET request from client.
	public void handleGET(CoapExchange ce) {
		ce.respond(ResponseCode.VALID, "GET worked!");
//		String content = ce.getRequestText();
//		ce.respond(ResponseCode.CONTENT, content);
		_Logger.info("Received GET request from client.");
	}
	
	//Handle POST request from client.
	public void handlePOST(CoapExchange ce){
		try {
			String payload = ce.getRequestText();
			_Logger.info("Received Json fromat Sensor Data:\n\t" + payload);
			SensorData convert = converJsontoSensorData(payload);
			String jsondata = converSensorDatatoJson(convert);
			ce.respond(ResponseCode.CREATED, "POST worked!");
//			ce.respond(ResponseCode.CREATED, payload + "\n\nConvert Json to SensorData Instance:" + "\n" 
//					+ convert.str() + "\n\nConvert SensorData Instance to Json: " + "\n" + jsondata);
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		ce.respond(ResponseCode.CREATED, payload);
        _Logger.info("Received POST request from client.");
    }
	
	//Handle PUT request from client.
	public void handlePUT(CoapExchange ce) {
//		ce.respond(ResponseCode.CHANGED, payload);
        ce.respond(ResponseCode.CHANGED, "PUT worked!");
        _Logger.info("Received PUT request from client.");
    }
	
	//Handle DELETE request from client.
	public void handleDELETE(CoapExchange ce) {
		this.delete();
        ce.respond(ResponseCode.DELETED, "DELETE worked!");
        _Logger.info("Received DELETE request from client.");
    }
	
//	// Handle PING request from client.
//	public void handlePING(CoapExchange ce) {
//		EmptyMessage em = new EmptyMessage(Type.RST);
//		ce.respond(em.toString());
//		_Logger.info("Received PING request from client.");
//	}
}
