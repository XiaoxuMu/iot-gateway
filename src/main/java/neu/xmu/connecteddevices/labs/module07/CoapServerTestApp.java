package neu.xmu.connecteddevices.labs.module07;

import java.io.IOException;

import com.google.gson.JsonIOException;

//CoapServerTestApp class creates a new instance of CoapServerConnector.
public class CoapServerTestApp {

	private static CoapServerTestApp _App;

	public static void main(String[] args)
     {
        _App = new CoapServerTestApp();
        try {
               _App.start();
        } catch (Exception e) {
               e.printStackTrace();
        } 
     }
	
	// private var's
    private CoapServerConnector _coapServer;
    
    // constructors
    public CoapServerTestApp()
    {
    	super(); 
    }
    
    // public methods
    public void start() throws JsonIOException, IOException
    {
       _coapServer = new CoapServerConnector();
       _coapServer.start();
    }
}
