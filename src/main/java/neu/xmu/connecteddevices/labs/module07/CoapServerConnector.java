package neu.xmu.connecteddevices.labs.module07;

import java.io.IOException;
import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;

import com.google.gson.JsonIOException;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

////CoapServerConnector class will instance the requisite handlers, and listen for connections.
public class CoapServerConnector {
	
	private static final Logger _Logger = Logger.getLogger(CoapServerConnector.class.getName());
	
	private String _protocol = ConfigConst.DEFAULT_COAP_PROTOCOL;
	private String _host = ConfigConst.DEFAULT_COAP_SERVER;
	private int _port = ConfigConst.DEFAULT_COAP_PORT;
	public String _serverAddr;

	//private var's
    private CoapServer _coapServer;
    
    // constructors
    public CoapServerConnector()
    {
    	super();
    	_serverAddr = _protocol + "://" + _host + ":" + _port;
    	_Logger.info("Using URL for broker connection: " + _serverAddr);
    }
    
    // public methods
    public void addResource(CoapResource resource)
    {
       if (resource != null) {
              _coapServer.add(resource);
       } 
    }
    
    public void start() throws JsonIOException, IOException 
    {
       if (_coapServer == null) {
    	   _Logger.info("Creating CoAP server instance and 'sensor' handler...");
           
           _coapServer = new CoapServer();
          //Instance a new resource handler.
           TempResourceHandler tempHandler = new TempResourceHandler("temp");
           _coapServer.add(tempHandler);
       }
       _Logger.info("Starting CoAP server...");
       _coapServer.start();
    }
    
    public void stop()
    {
    	_Logger.info("Stopping CoAP server...");
        _coapServer.stop();
    }
}

