package neu.xmu.connecteddevices.project;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.eclipse.paho.client.mqttv3.MqttException;

import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.xmu.connecteddevices.project.MqttClientConnector;

//TempActuatorSubscriberApp is to subscribe to the "TempActuator" variable using MQTT (ssl).
public class TempActuatorSubscriberApp {
	
	private String host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
	private String pemfilename = "/Users/Cassie/eclipse-workspace/iot-gateway/config/ubidots_cert.pem";
	private String token = "BBFF-8uhL2pnuekP3h0LJwsJYiNiP3cUAXi";

	//static
	private static TempActuatorSubscriberApp _App;
	
    public static void main(String[] args)
    {
       _App = new TempActuatorSubscriberApp();
       try {
              _App.start();
       } catch (Exception e) {
              e.printStackTrace();
       }
    }
    
    // params
    private MqttClientConnector _mqttClient;
    
    // constructors
    public TempActuatorSubscriberApp()
    {
    	super(); 
    }
    
    // public methods
    public void start() throws MqttException, KeyManagementException, 
    						   NoSuchAlgorithmException, KeyStoreException,
    						   CertificateException, IOException{
       _mqttClient = new MqttClientConnector(host, token, pemfilename);
       _mqttClient.connect();
       
       //Set up topic name.
       String topicName = "/v1.6/devices/new-device/temp-actuator/lv";
       // Subscribe to topic.
       _mqttClient.subscribeToTopic(topicName, 1); 
   }
}
