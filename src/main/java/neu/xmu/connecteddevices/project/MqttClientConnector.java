package neu.xmu.connecteddevices.project;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.UUID;
import java.util.logging.Logger;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.xmu.connecteddevices.common.ActuatorData;
import neu.xmu.connecteddevices.common.DataUtil;

public class MqttClientConnector implements MqttCallback{
	
	private String _protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	private String _host = ConfigConst.DEFAULT_MQTT_SERVER;
	private int _port = ConfigConst.DEFAULT_MQTT_PORT;
	private String _clientID = UUID.randomUUID().toString();
	public String _brokerAddr;
	private String _pemfilepath;
	private Boolean _isSecure = false;
	private String _userName = "BBFF-8uhL2pnuekP3h0LJwsJYiNiP3cUAXi";
	private String _password = "BBFF-8uhL2pnuekP3h0LJwsJYiNiP3cUAXi";
	private MqttClient _mqttClient;
	
	private static final Logger _Logger =
            Logger.getLogger(MqttClientConnector.class.getName());
	
	//Constructor : default
	public MqttClientConnector() {
		super();
		_Logger.info("Using client ID for broker connection: " + _clientID);
		_brokerAddr = _protocol + "://" + _host + ":" + _port;
		_Logger.info("Using URL for broker connection: " + _brokerAddr);
	}
	
	//Constructor: SSL
	public MqttClientConnector(String hostname, String token, String pemfilepath){
		super();
		_host = hostname;
		if(pemfilepath != null) {
			File file = new File(pemfilepath);
			if(file.exists()) {
				_protocol = ConfigConst.SECURE_MQTT_PROTOCOL;
				_host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
				_port = ConfigConst.SECURE_MQTT_PORT;
				_pemfilepath = pemfilepath;
				//Set a boolean flag within the connector class to 
				//indicate whether or not TLS is enabled
				_isSecure = true;
				_Logger.info("Using secure connention.");
			} else {
				_Logger.info("Using default connention.");
			}
		}
		_Logger.info("Using client ID for broker connection: " + _clientID);
		_brokerAddr = _protocol + "://" + _host + ":" + _port;
		_Logger.info("Using URL for broker connection: " + _brokerAddr);
	}
	
	//Build a connection with server
	public void connect() throws MqttException, KeyManagementException, NoSuchAlgorithmException, 
						  		 KeyStoreException, CertificateException, IOException {
		if(_mqttClient == null) {
			MemoryPersistence persistence = new MemoryPersistence();
			_mqttClient = new MqttClient(_brokerAddr, _clientID, persistence);
			
			MqttConnectOptions options = new MqttConnectOptions();
			options.setCleanSession(true);
			options.setKeepAliveInterval(10);
	
			if(_isSecure == true) {
				options.setUserName(_userName);
				options.setPassword(_password.toCharArray());
				secureConnect(options);
			}
	
			_Logger.info("Connecting to broker: " + _brokerAddr);
			_mqttClient.setCallback(this);
			_mqttClient.connect(options);
			_Logger.info("Connected to broker: " + _brokerAddr);
		}	
	}
	
	//Set up TLS/SSL support
	private void secureConnect(MqttConnectOptions option) 
			throws NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException, KeyManagementException {
		_Logger.info("Configuring TLS...");
		
		_protocol = ConfigConst.SECURE_MQTT_PROTOCOL;
		
		SSLContext sslcontext = SSLContext.getInstance(_protocol);
		KeyStore keystore = readKeyStore(_pemfilepath);
		TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init(keystore);
		sslcontext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());

		MqttConnectOptions options = new MqttConnectOptions();
		options.setSocketFactory(sslcontext.getSocketFactory());
	}
	
	//Read certificate from Keystore.
	private KeyStore readKeyStore(String pemfilepath) 
			throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
		KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
		FileInputStream fileinput = new FileInputStream(pemfilepath);
		BufferedInputStream bufferedinput =  new BufferedInputStream(fileinput);
		CertificateFactory certfac = CertificateFactory.getInstance("X.509");
		keystore.load(null);
		while(bufferedinput.available() > 0) {
			Certificate cert = certfac.generateCertificate(bufferedinput);
			keystore.setCertificateEntry("adk_store" + bufferedinput.available(), cert);
		}
		return keystore;
	}
	
	//Subscribe to a specific topic
	public void subscribeToTopic(String topicName, int qos) throws MqttException {
		_mqttClient.subscribe(topicName, qos);
		_Logger.info("Subscribe to topic: " + topicName);
	}

	//Publish the topic and payload to broker
	public void publishMessage(String topicName, int qos, byte[] bytes) 
			throws MqttPersistenceException, MqttException {
		_Logger.info("Publishing message to topic: " + topicName);
		//create a new MqttMessage and pass payload to the constructor.
		MqttMessage msg = new MqttMessage(bytes);
		//set qos level
		msg.setQos(qos);
		msg.setRetained(true);
		_mqttClient.publish(topicName, msg);
	}
	
	//Disconnect the connection
	public void disconnect() throws MqttException {
		_mqttClient.disconnect();
		_Logger.info("Disconnected with broker: " + _brokerAddr + "\n");
	}

	public void connectionLost(Throwable cause) {
		_Logger.info("Connection to MQTT broker lost!");
	}
	
	//If message arrived from the broker,output the topic name and payload.
	//Send the payload to device by MQTT
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		String payload = new String(message.getPayload());
		_Logger.info("Message received:\n\ttopic: "+ topic + "\n\tpayload: " + payload);
		
		//Set up ActuatorDate Instance
		ActuatorData actuatordata = new ActuatorData();
		actuatordata.command = 1;
		actuatordata.errCode = 0;
		actuatordata.statusCode = 1;
		actuatordata.stateData = "";
		actuatordata.val = Float.parseFloat(payload);
		actuatordata.name = "ActuatorData";
		
		_Logger.info(actuatordata.str());
		
		// set up a topic name 
        String topicName = "TempActuator";
		// set up payload.
		DataUtil actuator = new DataUtil();
		payload = actuator.toJsonFromActuatorData(actuatordata);
        
        _Logger.info("MQTT Payload: " + payload);
        
		MqttClientConnector _mqttClient = new MqttClientConnector();
		_mqttClient.connect();
        // publish message
        _mqttClient.publishMessage(topicName, 2, payload.getBytes());
        
	}

	//If the message is delivered, output a token.
	public void deliveryComplete(IMqttDeliveryToken token) {
		try {
			_Logger.info("Delivery completed: "+ token.getMessageId() +
								" - " + token.getResponse() + " - " + token.getMessage());
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}
}
