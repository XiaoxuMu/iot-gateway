package neu.xmu.connecteddevices.project;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.paho.client.mqttv3.MqttException;

import com.google.gson.JsonIOException;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.xmu.connecteddevices.common.DataUtil;
import neu.xmu.connecteddevices.common.SensorData;

//TempResourceHandler class is to handle the following requests from the client:
//1. Ping
//2. GET
//3. POST 
//4. PUT
//5. DELETE
public class TempResourceHandler extends CoapResource{
	
	private static final Logger _Logger =
            Logger.getLogger(TempResourceHandler.class.getName());
	
	//	private String content;
	private String _userName = "BBFF-8uhL2pnuekP3h0LJwsJYiNiP3cUAXi";
	private String _pemFileName = "/Users/Cassie/eclipse-workspace/iot-gateway/config/ubidots_cert.pem";
	private String _host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
	
	private MqttClientConnector _Client;
	
	//Constructors
	public TempResourceHandler() throws JsonIOException, IOException {
		super("temp", true);
	}
	
	//Set up resource name and payload.
	public TempResourceHandler(String name) throws JsonIOException, IOException {
		super(name);
		getAttributes().setTitle("temp");
		getAttributes().setObservable();
		setObservable(true);
		setObserveType(CoAP.Type.CON);
	}
	
	//Convert JSON to SensorData instance.
	public SensorData converJsontoSensorData(String data) {
		DataUtil datautil = new DataUtil();
		SensorData sensordata = datautil.toSensorDataFromJson(data);
		_Logger.info("Convert Json to SensorData Instance:\n\t" + sensordata.str());
		return sensordata;
	}
	
	//Convert the SensorData instance to JSON
	public String converSensorDatatoJson(SensorData convert) throws JsonIOException, IOException {
		DataUtil datautil = new DataUtil();
		String jsondata = datautil.toJsonFromSensorData(convert);
		_Logger.info("Convert SensorData Instance to Json:\n\t" + jsondata);
		return jsondata;
	}
	
	// Handle GET request from client.
	public void handleGET(CoapExchange ce) {
		ce.respond(ResponseCode.VALID, "GET worked!");
		_Logger.info("Received GET request from client.");
	}
	
	//Handle POST request from client.
	//Post is to create a resource
	//  Add MQTT part:
	//  After handlePOST, we can get the temp value from senseHat by COAP protocol
	//  And the value would get by ce.getRequestText() and send it to mqtt payload.
	//  Then, we use mqtt protocol--publish the value to the broker, cloud. 
	//  Final, the ubidots will get the sensor value from senseHat.
	public void handlePOST(CoapExchange ce){
		try {
			// Set the temperature to what we get from the Raspberry Pi
			String tempdata = ce.getRequestText();
			_Logger.info("Received tempData:\n\t" + tempdata);
			ce.respond(ResponseCode.CREATED, tempdata);
			
			_Client = new MqttClientConnector(_host, _userName, _pemFileName);
			_Client.connect();
			
			String topicName1 = "/v1.6/devices/new-device/temp-sensor";
			String topicName2 = "/v1.6/devices/new-device/avgtemp";
			String topicName3 = "/v1.6/devices/new-device/estimatedtemp";
			
			SensorData convert = converJsontoSensorData(tempdata);
			
			String payload1 = String.valueOf(convert.curValue);
			String payload2 = String.valueOf(convert.getAvgValue());
			//estimated temp acccording to sensor hat.
			float r = (float) (0.8 * convert.getAvgValue());
			String payload3 = String.valueOf(r);
			
			//publish payload with the topic in Qos level 0
			_Client.publishMessage(topicName1, 0, payload1.getBytes());
			_Client.publishMessage(topicName2, 0, payload2.getBytes());
			_Client.publishMessage(topicName3, 0, payload3.getBytes());
			_Client.disconnect();
	
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       _Logger.info("Received POST request from client.");
    }
	
	//Handle PUT request from client.
	public void handlePUT(CoapExchange ce) {
        ce.respond(ResponseCode.CHANGED, "PUT worked!");
        _Logger.info("Received PUT request from client.");
    }
	
	//Handle DELETE request from client.
	public void handleDELETE(CoapExchange ce) {
		this.delete();
        ce.respond(ResponseCode.DELETED, "DELETE worked!");
        _Logger.info("Received DELETE request from client.");
    }
	
}
