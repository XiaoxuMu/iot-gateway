package neu.xmu.connecteddevices.project;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.paho.client.mqttv3.MqttException;

import com.google.gson.JsonIOException;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.xmu.connecteddevices.common.DataUtil;
import neu.xmu.connecteddevices.common.SensorData;

//PressResourceHandler class is to handle the following requests from the client:
//1. Ping
//2. GET
//3. POST 
//4. PUT
//5. DELETE
public class PressResourceHandler extends CoapResource{
	
	private static final Logger _Logger =
            Logger.getLogger(PressResourceHandler.class.getName());
	
	//	private String content;
	private String _userName = "BBFF-8uhL2pnuekP3h0LJwsJYiNiP3cUAXi";
	private String _pemFileName = "/Users/Cassie/eclipse-workspace/iot-gateway/config/ubidots_cert.pem";
	private String _host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
	
	private MqttClientConnector _Client;
	
	//Constructors
	public PressResourceHandler() throws JsonIOException, IOException {
		super("press", true);
	}
	
	//Set up resource name and payload.
	public PressResourceHandler(String name) throws JsonIOException, IOException {
		super(name);
		getAttributes().setTitle("press");
		getAttributes().setObservable();
		setObservable(true);
		setObserveType(CoAP.Type.CON);
	}
	
	//Convert JSON to SensorData instance.
	public SensorData converJsontoSensorData(String data) {
		DataUtil datautil = new DataUtil();
		SensorData sensordata = datautil.toSensorDataFromJson(data);
		_Logger.info("Convert Json to SensorData Instance:\n\t" + sensordata.str());
		return sensordata;
	}
	
	//Convert the SensorData instance to JSON
	public String converSensorDatatoJson(SensorData convert) throws JsonIOException, IOException {
		DataUtil datautil = new DataUtil();
		String jsondata = datautil.toJsonFromSensorData(convert);
		_Logger.info("Convert SensorData Instance to Json:\n\t" + jsondata);
		return jsondata;
	}
	
	// Handle GET request from client.
	public void handleGET(CoapExchange ce) {
		ce.respond(ResponseCode.VALID, "GET worked!");
		_Logger.info("Received GET request from client.");
	}
	
	//Handle POST request from client.
	//Post is to create a resource
	//  Add MQTT part:
	//  After handlePOST, we can get the press value from senseHat by COAP protocol
	//  And the value would get by ce.getRequestText() and send it to mqtt payload.
	//  Then, we use mqtt protocol--publish the value to the broker, cloud. 
	//  Final, the ubidots will get the sensor value from senseHat.
	public void handlePOST(CoapExchange ce){
		try {
			// Set the temperature to what we get from the Raspberry Pi
			String pressdata = ce.getRequestText();
			_Logger.info("Received pressData:\n\t" + pressdata);
			ce.respond(ResponseCode.CREATED, pressdata);
			
			_Client = new MqttClientConnector(_host, _userName, _pemFileName);
			_Client.connect();
			
			String topicName1 = "/v1.6/devices/new-device/presssensor";
			SensorData convert = converJsontoSensorData(pressdata);
			String payload = String.valueOf(convert.curValue);
			//publish payload with the topic in Qos level 0
			_Client.publishMessage(topicName1, 0, payload.getBytes());
			_Client.disconnect();
			
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       _Logger.info("Received POST request from client.");
    }
	
	//Handle PUT request from client.
	public void handlePUT(CoapExchange ce) {
        ce.respond(ResponseCode.CHANGED, "PUT worked!");
        _Logger.info("Received PUT request from client.");
    }
	
	//Handle DELETE request from client.
	public void handleDELETE(CoapExchange ce) {
		this.delete();
        ce.respond(ResponseCode.DELETED, "DELETE worked!");
        _Logger.info("Received DELETE request from client.");
    }
	
}

