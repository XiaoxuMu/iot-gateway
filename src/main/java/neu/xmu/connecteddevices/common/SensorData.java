package neu.xmu.connecteddevices.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * SensorData Class stores the aggregated sensor data 
 * track the number of updates, maintain a running average, 
 * and track the min, max and current value. 
 */
public class SensorData
{
	public String name = "Temperature";
	public String timeStamp = updateTimeStamp();
	public float avgValue = 0.0f;
	public float minValue = 0.0f;
	public float maxValue = 0.0f;
	public float curValue = 0.0f;
	public float totValue = 0.0f;
	public int sampleCount = 0;
	
	public SensorData()
	{
		super(); 
	}
	
	public void addValue(float val)
	{
		updateTimeStamp();
		
		++this.sampleCount;
		
		this.curValue  = val;
		this.totValue += val;
		
		if (this.curValue < this.minValue) {
           this.minValue = this.curValue;
		}
		
		if (this.curValue > this.maxValue) {
           this.maxValue = this.curValue;
		}
		
		if (this.totValue != 0 && this.sampleCount > 0) {
           this.avgValue = this.totValue / this.sampleCount;
		} 
	}

	private String updateTimeStamp() {
		Date date = new Date();    
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);
	}
	
	public float getAvgValue() {
		return avgValue;
	}
	
	public float getValue() {
		return curValue;
	}
	
	public float getMinValue() {
		return minValue;
	}
	
	public float getMaxValue() {
		return maxValue;
	}
	
	public String str() {
	    String str = ("Name: " + name + "\n" + 
	                  "\tTime: " + updateTimeStamp() + "\n" +
	                  "\tCurrent: " + getValue() + "\n" +
	                  "\tAverage: " + getAvgValue() + "\n" +
	                  "\tSamples: " + sampleCount + "\n" +
	                  "\tMin: " + getMinValue() + "\n" +
	                  "\tMax: " + getMaxValue() + "\n" +
	                  "\tTotal: " + totValue
	                  );
		return str;
	}
	
	public String pringSensorData() {
		String sd = ("--------------------\nNew sensor readings: \n" + 
	                  "  name=" + getName() + ",timeStamp=" + updateTimeStamp() + 
	                  ",avgValue=" + getAvgValue() + ",minValue=" + getMinValue() + 
	                  ",maxValue=" + getMaxValue() + ",curValue=" + getValue()+ 
	                  ",totValue=" + totValue + ",sampleCount=" + sampleCount
                     );
        return sd;
	}

	public String getName() {
		return name;
	}

	public float getErrorCode() {
		return Math.abs(curValue - getAvgValue());
	}
	
	public void setSensorDataInstance() {
        avgValue = (float) 22.47;
        minValue = 0;
        maxValue = (float) 24.77;
        curValue = (float) 24.77;
        totValue= (float) 44.94;
        sampleCount = 2;
	}
	

}