package neu.xmu.connecteddevices.common;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import neu.xmu.connecteddevices.project.MqttClientConnector;

/**
 * DataUtil class is used to convert to JSON from SensorData and vice versa, 
 * as well as to JSON from ActuatorData, and vice versa.
 * @author Xiaoxu Mu
 */
public class DataUtil {

	private static final Logger _Logger =
            Logger.getLogger(DataUtil.class.getName());
	 //Accepts a SensorData reference as a parameter, converts it to JSON, and returns the String.
	 public String toJsonFromSensorData(SensorData sensorData) throws JsonIOException, IOException
     {
         String jsonData = null;
         if (sensorData != null) {
        	 // Create a new Gson object
             Gson gson = new Gson();
             //convert the Java object to json
             jsonData = gson.toJson(sensorData);
             //Write JSON String to file        
             FileWriter fileWriter = new FileWriter("/Users/Cassie/eclipse-workspace/iot-gateway/Data/SensorData.json");
             fileWriter.write(jsonData);
             fileWriter.close();
         }
//         System.out.println("\n From SensorData to Json: ");
//         System.out.println(jsonData);
         return jsonData;
     }
	 
	 //Accepts a JSON String as a parameter, converts it to a SensorData instance, and returns the SensorData object.
	 public SensorData toSensorDataFromJson(String jsonData)
     {
		 SensorData sensorData = null;
         if (jsonData != null && jsonData.trim().length() > 0) {
        	 // Create a new Gson object
             Gson gson = new Gson();
             //convert the json to Java object
             sensorData = gson.fromJson(jsonData, SensorData.class);
         }
//         System.out.println("\n From Json to SensorData: ");
//         System.out.println(sensorData);
         return sensorData;
     }
	 
	 //Accepts a String filename and path as a parameter. 
	 //It points to a JSON file that can be read and used to generate a SensorData instance.
	 public SensorData toSensorDataFromJsonFile(String filepath) throws JsonSyntaxException, JsonIOException, FileNotFoundException
     {
		 SensorData sensorData = null;
         if (filepath != null) {
        	 //Create a new Gson object
             Gson gson = new Gson();
             //Read the json file from path
             BufferedReader bufferedReader = new BufferedReader(new FileReader(filepath));
             //convert the json to Java object 
             sensorData = gson.fromJson(bufferedReader, SensorData.class);
         }
         System.out.println("\n From Json File to SensorData: ");
         //Printing the SensorData Details
         System.out.println(sensorData.pringSensorData());
         return sensorData;
     }
	 
	 //Accepts a ActuatorData reference as a parameter, converts it to JSON, and returns the String.
	 public String toJsonFromActuatorData(ActuatorData actuatorData) throws JsonIOException, IOException
     {
         String jsonData = null;
         if (actuatorData != null) {
        	 // Create a new Gson object
             Gson gson = new Gson();
             //convert the Java object to json
             jsonData = gson.toJson(actuatorData);
             //Write JSON String to file
             FileWriter fileWriter = new FileWriter("/Users/Cassie/eclipse-workspace/iot-gateway/Data/ActuatorData.json");
             fileWriter.write(jsonData);
             fileWriter.close();
         }
         _Logger.info("From ActuatorData to Json: " + jsonData);
         return jsonData;
     }
	 
	 //Accepts a JSON String as a parameter, converts it to a ActuatorData instance, and returns the ActuatorData object. 
     public ActuatorData toActuatorDataFromJson(String jsonData)
     {
         ActuatorData actuatorData = null;
         if (jsonData != null && jsonData.trim().length() > 0) {
        	 // Create a new Gson object
             Gson gson = new Gson();
             //convert the json to Java object
             actuatorData = gson.fromJson(jsonData, ActuatorData.class);
         }
         System.out.println("\n From Json to ActuatorData: ");
         System.out.println(actuatorData.pringActuatorData());
         return actuatorData;
     }
     
     //Accepts a String filename and path as a parameter with the assumption 
     //it points to a JSON file that can be read and used to generate a ActuatorData instance.
     public ActuatorData toActuatorDataFromJsonFile(String filepath) throws JsonSyntaxException, JsonIOException, FileNotFoundException
     {
    	 ActuatorData actuatorData = null;
         if (filepath != null) {
        	 //Create a new Gson object
             Gson gson = new Gson();
             //Read the json file from path
             BufferedReader bufferedReader = new BufferedReader(new FileReader(filepath));
             //convert the json to Java object 
             actuatorData = gson.fromJson(bufferedReader, ActuatorData.class);
         }
         System.out.println("\n From Json File to ActuatorData: ");
         System.out.println(actuatorData.pringActuatorData());
         return actuatorData;
     }
     

}

