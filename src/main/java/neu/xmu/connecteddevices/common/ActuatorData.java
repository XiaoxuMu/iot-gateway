package neu.xmu.connecteddevices.common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ActuatorData {

/**
 * ActuatorData Class stores the aggregated actuator data 
 * track the updates for time, error code, status code value. 
 */
	
//	public boolean hasError = false;
	public int command = 0;
	public int errCode = 0;
	public int statusCode = 0;
	public String stateData = null;
	public float val = 0.0f;
	public String name = "Actuator Data";
		
	public ActuatorData()
	{
		super(); 
	}
		
	public void updataData(ActuatorData ad)
	{
		this.setCommand(ad.getCommand());
		this.statusCode = ad.getStatusCode();
		this.setErrCode(ad.getErrorCode());
		this.stateData = ad.getStateData();
		this.val = ad.getValue();
	}

	private String updateTimeStamp() {
		Date date = new Date();    
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);
	}
		
	public int getCommand() {
		return command;
	}
		
	public int getStatusCode() {
		return statusCode;
	}
		
	public int getErrorCode() {
		return getErrCode();
	}
		
	public String getStateData() {
		return stateData;
	}
		
	public float getValue() {
		return val;
	}
		
	public String str() {
		String str = ("Actuator Data:\n" + 
		              "\tTime: " + updateTimeStamp() + "\n" +
		              "\tCommand: " + getCommand() + "\n" +
		              "\tStatusCode: " + getStatusCode() + "\n" +
		              "\tErrorCode: " + getErrorCode() + "\n" +
		              "\tStateData: " + getStateData() + "\n" +
		              "\tValue: " + getValue() 
		             );
		return str;
	}

	public String getName() {
			return name;
	}
	
	public String pringActuatorData() {
        String ad = ("\nNew actuator readings: \n" + "  name=" + getName() + 
        			 ",timeStamp="+ updateTimeStamp() + ",command=" + getCommand() + 
                     ",errCode=" + getErrorCode() + ",statusCode=" + getStatusCode() +
                     ",stateData=" + getStateData()+ ",val=" + getValue()
                     );
        return ad;
	}

	public void setCommand(int command) {
		this.command = command;
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}



}


