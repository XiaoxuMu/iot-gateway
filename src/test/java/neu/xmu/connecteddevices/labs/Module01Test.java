/**
 * 
 */
package neu.xmu.connecteddevices.labs;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import neu.xmu.connecteddevices.labs.module01.SystemCpuUtilTask;
import neu.xmu.connecteddevices.labs.module01.SystemMemUtilTask;

/**
 * Test class for all requisite Module01 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app
 * 5) Include a screen shot of the report when you submit your assignment
 */
public class Module01Test
{
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		System.out.println("Start test.");
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		System.out.println("End test.");
	}
	
	
	@Test
	public void test_cpuvalue()
	{
		System.out.println("Test CPU utilization percent float value.");
		float cpudata = SystemCpuUtilTask.getDataFromSensor();
		assertTrue(cpudata >=0.0);
		assertTrue(cpudata <= 100.0);
	}
	
	@Test
	public void test_memvalue()
	{
		System.out.println("Test Menory utilization percent float value.");
		float memdata = SystemMemUtilTask.getDataFromSensor();
		assertTrue(memdata >= 0.0);
		assertTrue(memdata <= 100.0);
	}
	
}
