/**
 * 
 */
package neu.xmu.connecteddevices.labs;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import neu.xmu.connecteddevices.common.DataUtil;
import neu.xmu.connecteddevices.common.SensorData;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

/**
 * Test class for all requisite Module05 functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app
 * 5) Include a screen shot of the report when you submit your assignment
 */
public class Module05Test
{
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		System.out.println("Start test.");
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		System.out.println("End test.");
	}
	
	@Test
	public void testReadSensorDataFromJsonFile() throws JsonSyntaxException, JsonIOException, FileNotFoundException
	{
		DataUtil datautil = new DataUtil();
		SensorData sensorData = new SensorData();
		System.out.println("Test read Sensor Data From Json File.");
	}
	
}
