/**
 * 
 */
package neu.xmu.connecteddevices.common;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

/**
 * Test class for all requisite DataUtil functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app
 * 5) Include a screen shot of the report when you submit your assignment
 */
public class DataUtilTest
{
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		System.out.println("\nStart test.");
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		System.out.println("\nEnd test.");
	}
	
	@Test
	public void testSensorDatajson() throws JsonSyntaxException, JsonIOException, IOException
	{
		DataUtil datautil = new DataUtil();
		SensorData sensorData = new SensorData();
		System.out.println("\nTest toJsonFromSensorData() fuction.");
		
		System.out.println("\nTest toSensorDataFromJson() fuction.");
	

	}
	
}
