/**
 * 
 */
package neu.xmu.connecteddevices.project;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.xmu.connecteddevices.labs.module06.MqttClientConnector;

/**
 * Test class for all requisite Project functionality.
 * 
 * Instructions:
 * 1) Rename 'testSomething()' method such that 'Something' is specific to your needs; add others as needed, beginning each method with 'test...()'.
 * 2) Add the '@Test' annotation to each new 'test...()' method you add.
 * 3) Import the relevant modules and classes to support your tests.
 * 4) Run this class as unit test app
 * 5) Include a screen shot of the report when you submit your assignment
 */
public class ProjectTest
{
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		System.out.println("Start test.");
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		System.out.println("End test.");
	}
	
	@Test
	public void testSomething()
	{
		System.out.println("Test broker address");
		String host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
		String pemfilename = "/Users/Cassie/eclipse-workspace/iot-gateway/config/ubidots_cert.pem";
		String token = "BBFF-8uhL2pnuekP3h0LJwsJYiNiP3cUAXi";
		MqttClientConnector broker = new MqttClientConnector(host, token, pemfilename);
	}
	
}
